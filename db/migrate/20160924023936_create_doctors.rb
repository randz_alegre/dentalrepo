class CreateDoctors < ActiveRecord::Migration
  def change
    create_table :doctors do |t|
    	t.string :first_name, null:false
    	t.string :middle_name, null:false
    	t.string :last_name, null:false
    	t.date	 :birthdate, null:false
    	t.string :prc_number, null:false
    	t.string :contact_num, null:false
    	t.belongs_to :clinic, index:true
      	t.timestamps null: false
    end
  end
end
