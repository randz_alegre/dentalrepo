class CreatePatients < ActiveRecord::Migration
  def change
    create_table :patients do |t|
		t.string  	:first_name, null:false
    	t.string  	:middle_name, null:false
    	t.string  	:last_name, null:false
    	t.string  	:home_address, null:false
    	t.date	  	:birthdate, null:false
    	t.integer 	:gender, null:false
    	t.string	:marital_status, null:false
    	t.string	:occupation, null:false
    	t.string  	:contact_number, null:false
      	t.timestamps null: false
    end
  end
end
