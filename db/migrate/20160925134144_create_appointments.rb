class CreateAppointments < ActiveRecord::Migration
  def change
    create_table :appointments do |t|
    	t.string :first_name, null:false
    	t.string :middle_name, null:false
    	t.string :last_name, null:false
    	t.string :contact_number, null:false
    	t.belongs_to :clinic, index:true
      	t.timestamps null: false
    end
  end
end
