class AddDateColumnInAppointment < ActiveRecord::Migration
  def change
  	add_column :appointments, :preferred_date, :datetime
  end
end
