class CreatePatientrecords < ActiveRecord::Migration
  def change
    create_table :patientrecords do |t|
    	t.datetime :datetime_schedule
    	t.string :occlusion
    	t.string :periodontal_condition
    	t.string :oral_hygiene
    	t.string :denture_location
    	t.date 	 :denture_since
    	t.string :abnormalities
    	t.string :general_condition
    	t.string :nature_of_treatment
    	t.string :allergies
    	t.string :bleeding_history
    	t.string :chronic_ailments
    	t.string :blood_pressure
    	t.string :drug_taken
    	t.belongs_to :clinic, index: true
    	t.belongs_to :patient, index: true
    	t.timestamps null: false
    end
  end
end
