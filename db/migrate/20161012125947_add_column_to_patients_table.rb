class AddColumnToPatientsTable < ActiveRecord::Migration
  def change
  	add_column :patients, :home_city, :string
  	change_column :patients, :home_city, :string, :null => false
  	add_column :patients, :home_province, :string
  	change_column :patients, :home_province, :string, :null => false
  end
end
