class AppointmentsController < ApplicationController
	before_action :get_clinic

	def index

		@appointments = Appointment.where("clinic_id == ?", @clinic.id)

	end

	def new
		@appointment = Appointment.new
	end

	def edit
		@appointment = @clinic.appointments.find(params[:id])
	end

	def show

		begin
			@appointment = @clinic.appointments.find(params[:id])
		
			@patient = Patient.where("first_name = ? AND last_name = ?", @appointment.first_name, @appointment.last_name).first
			

			unless @patient
				@patient = Patient.new
				@patient.first_name = @appointment.first_name
				@patient.last_name = @appointment.last_name
				@patient.middle_name = @appointment.middle_name
				@patient.contact_number = @appointment.contact_number
			end

			@patientRecord = @patient.patientrecords.new
			@patientRecord.datetime_schedule = @appointment.preferred_date

			rescue ActiveRecord::RecordNotFound
				redirect_to clinics_path, :flash => { :alert => "No Record Found" } 
		end
		

	end

	def update
		@appointment = @clinic.appointments.find(params[:id])

		if @appointment.update(appointment_params) 
			redirect_to clinics_path
		else
			render 'edit'
		end

	end
	
	def create
		@appointment = Appointment.new(appointment_params)
		@appointment.clinic_id = @clinic.id

		if @appointment.save
			redirect_to clinics_path
		else
			render 'new'
		end

	end

	private

	def get_clinic
		@user = User.find(current_user.id)
		@clinic = @user.clinic
	end

	def appointment_params
		params.require(:appointment).permit(:first_name, :middle_name, :last_name, :contact_number, :preferred_date)
	end
end
