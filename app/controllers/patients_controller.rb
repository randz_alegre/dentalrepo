class PatientsController < ApplicationController

	before_action :get_clinic

	def index
		begin
			@records = Patientrecord.where("clinic_id = ?", @clinic.id)

		rescue ActiveRecord::RecordNotFound
			redirect_to clinics_path 		
		end
	end

	def create
		@appointment = Appointment.find(params[:appointment_id])
		@patient = Patient.new(patient_params)

		if @patient.save
			redirect_to clinics_path
		else
			render 	:template => 'appointments/show'
		end


	end


	private

	def patient_params
		params.require(:patient).permit(:first_name, :middle_name, :last_name, :birthdate, :gender, :home_address, :home_city, :home_province , :contact_number, :marital_status, :occupation);
	end


end
