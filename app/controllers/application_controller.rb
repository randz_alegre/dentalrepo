class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  before_action :authenticate_user!

  
  def get_clinic
		@user = User.find(current_user.id)
		@clinic = @user.clinic
	end
end
