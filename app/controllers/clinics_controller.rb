class ClinicsController < ApplicationController
	

  def index
    @user = User.find(current_user.id)
    @clinic = @user.clinic
    
  	if @clinic == nil
  		redirect_to new_clinic_path
  	end

    @appointments = Appointment.where("clinic_id == ? AND preferred_date >= ?",  @clinic, Time.zone.now.beginning_of_day).order(:preferred_date => :asc)
  	
  end

  def new
  	checkClinicExist

  	@clinic = Clinic.new
  end

  def create
  	checkClinicExist
  	@clinic = Clinic.new(clinic_params)
  	@clinic.user_id = current_user.id
  	if @clinic.save
  		redirect_to clinics_path
  	else
  		render 'new'
  	end
  end

  private
  	def clinic_params
		params.require(:clinic).permit(:clinic_name, :address, :city, :province, :telephone_number, :mobile_number, :description)
	end


	def checkClinicExist
		user = User.find(current_user.id)
	  	if user.clinic == nil
	  		return false
	  	else
	  		redirect_to clinics_path
		end
	end
end
