class DoctorsController < ApplicationController
	before_action :get_clinic

	def index
		
		@doctors = Doctor.where("clinic_id == ?", @clinic.id)
	end

	def new
		user = User.find(current_user.id)
		@clinic = user.clinic
		@doctor = Doctor.new
	end

	def create
		user = User.find(current_user.id)
		@clinic = user.clinic
		@doctor = Doctor.new(doctor_params)
		@doctor.clinic_id = @clinic.id

		if @doctor.save
			redirect_to clinic_doctors_path
		else
			render 'new'
		end
	end

	def edit
		@doctor = @clinic.doctors.find(params[:id])
	end

	def update
		@doctor = @clinic.doctors.find(params[:id])

		if @doctor.update(doctor_params)
			redirect_to clinic_doctors_path
		else
			render 'edit'
		end

	end

	private

	def get_clinic
		@user = User.find(current_user.id)
		@clinic = @user.clinic
	end
	
	def doctor_params
		params.require(:doctor).permit(:first_name, :middle_name, :last_name, :birthdate, :prc_number, :contact_num);
	end
end
