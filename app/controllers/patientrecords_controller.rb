class PatientrecordsController < ApplicationController
	before_action :get_clinic
	def create
		@appointment = @clinic.appointments.find(params[:appointment_id])
		
		@patient = Patient.where("first_name = ? AND last_name = ?", @appointment.first_name, @appointment.last_name).first
		@patientRecord = @patient.patientrecords.new(record_params)
		@patientRecord.clinic_id = @clinic.id

		if @patientRecord.save
			redirect_to clinics_path
		else
			render 'appointments/show'
		end
	end

	def edit
		
	end

	def update

	end

	private

	def record_params
		params.require(:patientrecord).permit(
				:datetime_schedule,
				:occlusion,
				:periodontal_condition,
				:oral_hygiene,
				:denture_location,
				:denture_since,
				:abnormalities,
				:general_condition,
				:nature_of_treatment,
				:allergies,
				:bleeding_history,
				:chronic_ailments,
				:blood_pressure,
				:drug_taken
				)
	end

	def get_clinic
		@user = User.find(current_user.id)
		@clinic = @user.clinic
	end
end
