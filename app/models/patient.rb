class Patient < ActiveRecord::Base
	has_many :patientrecords
	has_many :clinics , through: :patientrecords
	validates 	:first_name, 
				:middle_name, 
				:last_name, 
				:birthdate, 
				:home_address, 
				:home_city, 
				:home_province,
				:marital_status,
				:occupation,
				presence: true
  	validates :gender, presence: true
	validates :contact_number, length: {is: 11, message: "should have 11 digits"}
end
