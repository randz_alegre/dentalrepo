class Doctor < ActiveRecord::Base
	belongs_to :clinic

	validates :first_name, :middle_name, :last_name, :birthdate, :prc_number, :contact_num, presence: true
	validates :contact_num, length: {is: 11, message: "should have 11 digits"}
end
