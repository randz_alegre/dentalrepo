class Clinic < ActiveRecord::Base

	belongs_to :user
	has_many :doctors
	has_many :appointments
	has_many :patientrecords
	has_many :patients, through: :patientrecords

	
	validates :clinic_name, :address, :city, :province, :telephone_number, :mobile_number, :description, presence: true
	validates :telephone_number, length: {is: 7, message: "should have 7 digits"}
	validates :mobile_number, length: {is: 11, message: "should have 11 digits"}
	validates :description, length:{maximum: 500}
end
