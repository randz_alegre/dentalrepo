class Appointment < ActiveRecord::Base
	belongs_to :clinic

	validates :first_name, :middle_name, :last_name, :contact_number, presence: true
	validates :contact_number, length: {is: 11, message: "should have 11 digits"}
end
